// ==UserScript==
// @name         Nexus Chat+
// @version      1.0.0
// @description  Movable Nexus mode chat
// @author       PsCid
// @match        https://animemusicquiz.com/*
// @updateURL    https://gitlab.com/amq-pscid/nexus-chat-plus/-/raw/master/Nexus%20Chat+.user.js
// @grant        none
// ==/UserScript==

if (document.getElementById("startPage")) return;

// -----------------------------
// Add CSS
var styleSheet = document.createElement("style")
styleSheet.innerText = styles
document.head.appendChild(styleSheet)

// -----------------------------
// Add classes
var chatElement = document.getElementById("nexusCoopContainer");
chatElement.classList.add("move");

var chatTabElement = document.getElementById("nexusCoopTabContainer");
chatTabElement.style.maxWidth = '300px';

var chatMainElement = document.getElementById("nexusCoopMainContainer");
chatMainElement.style.height = '200px';

// -----------------------------
// Drag
var div = document.querySelector('.move'),
    x = 0,
    y = 0,
    mousedown = false;

div.addEventListener('mousedown', function (e) {
    mousedown = true;
    x = div.offsetLeft - e.clientX;
    y = div.offsetTop - e.clientY;
}, true);

div.addEventListener('mouseup', function (e) {
    mousedown = false;
}, true);

div.addEventListener('mousemove', function (e) {
    if (mousedown) {
        div.style.left = e.clientX + x + 'px';
        div.style.top = e.clientY + y + 'px';
    }
}, true);

// -----------------------------
// CSS Styles
var styles = `
 .move {
     position: absolute;
     width: 150px;
     height: 150px;
     left: 0;
     top: 0;
     background-color: #333;
 }
`


